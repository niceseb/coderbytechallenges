def LetterChanges(str):
    list_ascii = []

    for i in str:

        if i.isspace() or not i.isalpha():
            list_ascii.append(i)
            continue
        else:
            n = ord(i)
            n += 1
            list_ascii.append(chr(n))

    return ''.join(list_ascii).replace('o', 'O').replace('a', 'A').replace('e', 'E').replace('i', 'I').replace('u', 'U')


print LetterChanges(raw_input("Please provide input text:"))
print LetterChanges(raw_input())
